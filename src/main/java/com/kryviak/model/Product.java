package com.kryviak.model;

public class Product {
    private int product_id;
    private String name;
    private int price;
    private int kilogram;

    public Product(int product_id, String name, int price, int kilogram) {
        this.product_id = product_id;
        this.name = name;
        this.price = price;
        this.kilogram = kilogram;
    }

    @Override
    public String toString() {
        return String.format("id: %-5s name: %-15s price: %-5s kilogram: %-5s", product_id, name, price, kilogram);
    }
}
