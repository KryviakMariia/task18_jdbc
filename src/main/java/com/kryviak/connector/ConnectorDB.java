package com.kryviak.connector;

import java.sql.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ConnectorDB {
    private static Logger logger = LogManager.getLogger(ConnectorDB.class);
    private static final String URL = "jdbc:mysql://localhost:3306/shop?serverTimezone=UTC&useSSL=false";
    private static final String USER = "root";
    private static final String PASSWORD = "1994";

    private Connection connection = null;
    private Statement statement = null;
    public ResultSet resultSet = null;

    public Connection connectToDB() {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            connection = DriverManager.getConnection(URL, USER, PASSWORD);
        } catch (SQLException | ClassNotFoundException ex) {
            logger.error(ex);
        }
        return connection;
    }

    public void closeConnections() {
        if (resultSet != null) try {
            resultSet.close();
        } catch (SQLException e) {
            logger.error(e);
        }
        if (statement != null) try {
            statement.close();
        } catch (SQLException e) {
            logger.error(e);
        }
        if (connection != null) try {
            connection.close();
        } catch (SQLException e) {
            logger.error(e);
        }
    }
}