package com.kryviak.menu;

import com.kryviak.DAO.ChangeDB;
import com.kryviak.connector.ConnectorDB;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.SQLException;
import java.util.InputMismatchException;
import java.util.Scanner;

public class PrintMenu {
    private static final int CLEAR_MENU_VALUE = 6;
    private static Logger logger = LogManager.getLogger(PrintMenu.class);
    private ChangeDB changeDB = new ChangeDB();
    private ConnectorDB dbConnector = new ConnectorDB();
    private Scanner input = new Scanner(System.in);
    private int selection;
    private boolean flag = false;

    public void printMenu() {
        while (!flag || selection != 0) {
            logger.info("Choose operation: \n'1' get all\n'2' insert\n'3' update\n'4' delete\n'5' get information about the table\n'0' exit");
            try {
                selection = input.nextInt();
                flag = true;
            } catch (InputMismatchException e) {
                logger.error("Please input only number value");
                selection = CLEAR_MENU_VALUE;
                String flush = input.next();
            }
            chooseOperation();
        }
        dbConnector.closeConnections();
    }

    private void chooseOperation() {
        switch (selection) {
            case 1:
                try {
                    changeDB.getAll(dbConnector.connectToDB().createStatement(), dbConnector.resultSet);
                } catch (SQLException e) {
                    logger.error(e);
                }
                break;
            case 2:
                changeDB.insertData(dbConnector.connectToDB());
                break;
            case 3:
                changeDB.updateData(dbConnector.connectToDB());
                break;
            case 4:
                changeDB.deleteDateFromDB(dbConnector.connectToDB());
                break;
            case 5:
                changeDB.getPrimaryKey(dbConnector.connectToDB(), changeDB.getColumnCountFromTable(changeDB.getInformationAboutTable(dbConnector.connectToDB())));
                changeDB.printInformationAboutTable(changeDB.getInformationAboutTable(dbConnector.connectToDB()), changeDB.getColumnCountFromTable(changeDB.getInformationAboutTable(dbConnector.connectToDB())));
                break;
        }
    }
}