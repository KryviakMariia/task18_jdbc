package com.kryviak.DAO.constants;

public enum ConstDB {
    INSERT("INSERT INTO shop.product (name, price, count_kilogram) VALUE (?, ?, ?)"),
    UPDATE("UPDATE shop.product SET name = ? WHERE name = ?"),
    DELETE ("DELETE FROM shop.product WHERE name = ?"),
    GET_ALL("SELECT * FROM product");
    private String queries;

    ConstDB(String queries) {
        this.queries = queries;
    }

    public String getQueries() {
        return queries;
    }
}
