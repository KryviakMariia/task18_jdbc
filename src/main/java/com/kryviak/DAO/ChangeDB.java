package com.kryviak.DAO;

import com.kryviak.DAO.constants.ConstDB;
import com.kryviak.model.Product;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.util.Scanner;

public class ChangeDB implements com.kryviak.DAO.ChangeableDB {
    private static Logger logger = LogManager.getLogger(ChangeDB.class);
    private Scanner input = new Scanner(System.in);
    private String productName;
    private int productPrice;
    private int productWeight;

    public void insertData(Connection connection) {
        logger.info("Please input a new product name: ");
        productName = input.next();
        logger.info("Please input a new product price: ");
        productPrice = input.nextInt();
        logger.info("Please input a new product weight: ");
        productWeight = input.nextInt();

        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(ConstDB.INSERT.getQueries());
            preparedStatement.setString(1, productName);
            preparedStatement.setInt(2, productPrice);
            preparedStatement.setInt(3, productWeight);
            int n = preparedStatement.executeUpdate();
        } catch (SQLException e) {
            logger.error(e);
        }
        logger.info("New product '" + productName + "' was added successfully");
    }

    public void updateData(Connection connection) {
        logger.info("Please input a product name that you want to change: ");
        productName = input.next();
        logger.info("Please input new product name: ");
        String newProductName = input.next();
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(ConstDB.UPDATE.getQueries());
            preparedStatement.setString(1, newProductName);
            preparedStatement.setString(2, productName);
            int n = preparedStatement.executeUpdate();
        } catch (SQLException e) {
            logger.error(e);
        }
        logger.info("New product name '" + newProductName + "' was added successfully");
    }

    public void deleteDateFromDB(Connection connection) {
        logger.info("Please input a product name that you want to delete: ");
        productName = input.next();
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(ConstDB.DELETE.getQueries());
            preparedStatement.setString(1, productName);
            int n = preparedStatement.executeUpdate();
        } catch (SQLException e) {
            logger.error(e);
        }
        logger.info("Product name " + productName + "was deleted successfully");
    }

    public void getAll(Statement statement, ResultSet resultSet) {
        try {
            resultSet = statement.executeQuery(ConstDB.GET_ALL.getQueries());
            logger.info("Table: " + "product");
            while (resultSet.next()) {
                int productId = resultSet.getInt("product_id");
                productName = resultSet.getString("name");
                productPrice = resultSet.getInt("price");
                productWeight = resultSet.getInt("count_kilogram");

                Product product = new Product(productId, productName, productPrice, productWeight);
                logger.info(product.toString());
            }
        } catch (SQLException e) {
            logger.error(e);
        }
    }

    public void getPrimaryKey(Connection connection, int countColumn) {
        DatabaseMetaData meta = null;
        String primaryKey = null;
        try {
            meta = connection.getMetaData();
            ResultSet rs1 = meta.getTables(null, null, "product", new String[]{"TABLE"});
            rs1 = meta.getPrimaryKeys(null, null, "product");
            while (rs1.next())
                primaryKey = rs1.getString(countColumn);
        } catch (SQLException e) {
            logger.error(e);
        }
        logger.info("Table: product\nPrimary key: " + primaryKey);
    }

    public int getColumnCountFromTable(ResultSetMetaData rsmd) {
        int columnCount = 0;
        try {
            columnCount = rsmd.getColumnCount();
        } catch (SQLException e) {
            logger.error(e);
        }
        return columnCount;
    }

    public ResultSetMetaData getInformationAboutTable(Connection connection) {
        ResultSet rs = null;
        ResultSetMetaData rsmd = null;
        try {
            rs = connection.createStatement().executeQuery(ConstDB.GET_ALL.getQueries());
            rsmd = rs.getMetaData();
        } catch (SQLException e) {
            logger.error(e);
        }
        return rsmd;
    }

    public void printInformationAboutTable(ResultSetMetaData rsmd, int columnCount) {
        try {
            logger.info("No. of columns: " + columnCount);
            for (int i = 1; i <= columnCount; i++) {
                logger.info("Name of " + i + " column: '" + rsmd.getColumnName(i) + "' Type: " + rsmd.getColumnTypeName(i));
            }
        } catch (SQLException e) {
            logger.error(e);
        }
    }
}