package com.kryviak.DAO;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;

public interface ChangeableDB {
    void insertData(Connection connection);

    void updateData(Connection connection);

    void deleteDateFromDB(Connection connection);

    void getAll(Statement statement, ResultSet resultSet);

    void getPrimaryKey(Connection connection, int countColumn);

    ResultSetMetaData getInformationAboutTable(Connection connection);

    void printInformationAboutTable(ResultSetMetaData rsmd, int columnCount);
}
