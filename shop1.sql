SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

CREATE SCHEMA IF NOT EXISTS `shop` DEFAULT CHARACTER SET utf8 ;
USE `shop` ;

-- -----------------------------------------------------
-- Table `shop`.`address`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `shop`.`address` (
  `country` VARCHAR(25) NOT NULL,
  PRIMARY KEY (`country`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `shop`.`customer`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `shop`.`customer` (
  `customer_id` INT(11) NOT NULL AUTO_INCREMENT,
  `surname` VARCHAR(25) NOT NULL,
  `name` VARCHAR(25) NOT NULL,
  `country` VARCHAR(25) NOT NULL,
  `phone_number` VARCHAR(10) NULL DEFAULT NULL,
  `money` INT(11) NOT NULL,
  PRIMARY KEY (`customer_id`),
  INDEX `country` (`country` ASC) VISIBLE,
  CONSTRAINT `customer_ibfk_1`
    FOREIGN KEY (`country`)
    REFERENCES `shop`.`address` (`country`)
    ON DELETE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 6
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `shop`.`product`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `shop`.`product` (
  `product_id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `price` INT(11) NOT NULL,
  `count_kilogram` INT(11) NOT NULL,
  PRIMARY KEY (`product_id`))
ENGINE = InnoDB
AUTO_INCREMENT = 14
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `shop`.`customer_has_product`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `shop`.`customer_has_product` (
  `customer_id` INT(11) NOT NULL,
  `product_id` INT(11) NOT NULL,
  PRIMARY KEY (`customer_id`, `product_id`),
  INDEX `product_id` (`product_id` ASC) VISIBLE,
  CONSTRAINT `customer_has_product_ibfk_1`
    FOREIGN KEY (`customer_id`)
    REFERENCES `shop`.`customer` (`customer_id`)
    ON DELETE CASCADE,
  CONSTRAINT `customer_has_product_ibfk_2`
    FOREIGN KEY (`product_id`)
    REFERENCES `shop`.`product` (`product_id`)
    ON DELETE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

USE `shop` ;

-- -----------------------------------------------------
-- procedure insert_customer_product
-- -----------------------------------------------------

DELIMITER $$
USE `shop`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `insert_customer_product`(
IN surname_custome_in varchar(25),
IN product_name_in varchar(45)
)
BEGIN
	DECLARE message varchar(40);
    
  -- checks for present Surname
  IF NOT EXISTS( SELECT * FROM customr WHERE surname = surname_custome_in)
  THEN SET message = 'Surname is absent';
    
  -- checks for present Book
	ELSEIF NOT EXISTS( SELECT * FROM product WHERE name = product_name_in)
		THEN SET message = 'Product is absent';
    
  -- checks if there are this combination already
	ELSEIF EXISTS( SELECT * FROM customer_has_product 
		WHERE customer_id = (SELECT customer_id FROM customer WHERE surname = surname_customer_in)
        AND product_id = (SELECT product_id FROM product WHERE name = product_name_in)
        )
        THEN SET message = 'This Person already has this book';
	
  -- checks whether there is still such a book
	ELSEIF (SELECT count_kilogram FROM product WHERE name=product_name_in) 
    <= (SELECT COUNT(*) FROM customer_has_product WHERE product_id=(SELECT product_id FROM product WHERE name = product_name_in))
    THEN SET message = 'There are no this Book already';
    
    -- makes insert
    ELSE 
		INSERT customer_has_product (customer_id, product_id) 
        Value ( (SELECT customer_id FROM customer WHERE surname = surname_customer_in),
			     (SELECT product_id FROM product WHERE name = product_name_in) );
		SET message = 'OK';		 

	END IF;

	SELECT message AS message;

END$$

DELIMITER ;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
